#!/usr/bin/python
import asyncio
from bitstring import BitStream
import configparser
from datetime import datetime, timedelta, timezone
import logging
import math
import os
import os.path
import pytz
import requests
import struct
import ssl
import sys
import time
from tzlocal import get_localzone

#
# All the decoder functions must return the following data
# records in the following format:
#
# MEASUREMENT = "measurement_name"
# VALUE = "key=value,key=value"
# VALUETS = "key=value,key=value timestamp_ns"
# TAG = "key=value"
# RECORD = ( MEASUREMENT, [ TAG, TAG, TAG, ... ], VALUETS or VALUE)
# return [ RECORD, RECORD, ... ]
# 

def load_node_tags(node_id):
    """Load node tags from an ini file {ENV[CFGROOT]}/nodes/{node_id}

    This looks for an ini structured file with [tags] section
    and adds all key=value pairs from it as tags to all database
    entries created for the specific `node_id`.
    """
    root_cfg_dir = os.environ.get("CFGROOT", os.getcwd())
    cfg_dir = os.path.join(root_cfg_dir, "nodes")
    node_file = os.path.join(cfg_dir, str(node_id) + ".ini")
    if not os.path.exists(node_file):
        return {}

    try:
        parser = configparser.ConfigParser()
        parser.read(node_file)
        return parser.items("tags") if parser.has_section("tags") else {}
    except configparser.Error as ex:
        print("Configuration for {} broken in {}: {}".format(node_id, node_file, ex))
        return {}


# Temperature node
def decode_101(packet):
    temp, vin, counter = struct.unpack(">HBL", packet[:7])
    line = "core_vin={},core_temp={},counter={}".format(
                vin + 120,
                temp - (1 << 15),
                counter)
    status_line = "alive=1,core_vin={}".format(vin + 120)
    return [("sensors", [], line), ("status", [], status_line)]


# GSM status
def decode_102(packet):
    rssi, tcp_counter = struct.unpack(">BH", packet[:3])
    line = "rssi={},tcp_counter={}".format(
                rssi,
                tcp_counter)
    status = ["alive=1", line]

    if len(packet) > 3:
        failed_counter, country_code, network_code, area_code, cell_id = struct.unpack(">HHHHH", packet[3:13])
        ext_line = "tcp_failed={},country={},network={},area={},cell={}".format(
            failed_counter,
            country_code,
            network_code,
            area_code,
            cell_id)
        status.append(ext_line)

    return [("sensors", [], line), ("status", [], 
                                    ",".join(status))]

def _techem_date_to_ts(d):
    year = 2000 + ((d >> 9) & 0x3f)
    month = (d >> 5) & 0x0f
    day = d & 0x1f
    date = pytz.timezone("Europe/Prague").localize(datetime(year, month, day))
    date = date.astimezone(tz=timezone.utc) # convert into utc
    return int(date.timestamp() * 1000000000) # nanoseconds

WMBUS_TYPES = {
    0x80: "heat",
    0x62: "hot-water",
    0x72: "cold-water",
}

# wMbus Techem status
def decode_105(packet):
    if packet[0] == 0x80:
        records = []
        device, dateword, t1, t2, value = struct.unpack(">BHHHL", packet[:11])
        labels = ["medium={}".format(WMBUS_TYPES.get(device, hex(device)))]
        records.append(("status", labels, "alive=1"))
        records.append(("sensors",
                        labels,
                        "t1={},t2={}".format(
                            t1 - (1 << 15),
                            t2 - (1 << 15))))
        records.append(("sensors",
                        labels,
                        "value={} {}".format(
                            value,
                            _techem_date_to_ts(dateword))))
        return records
    else:
        device, dateword, value = struct.unpack(">BHL", packet[:7])
        labels = ["medium={}".format(WMBUS_TYPES.get(device, hex(device)))]
        return [("status", labels, "alive=1"),
                ("sensors",
                 labels,
                 "value={} {}".format(
                    value,
                    _techem_date_to_ts(dateword)))]

# Environmental node
def decode_106(packet):
    stream = BitStream(packet)
    humidity = stream.read("uint:8") * 5
    core_vin = stream.read("uint:8") + 120
    co2_exp = stream.read("uint:2")
    co2_value = stream.read("uint:6")
    temp = stream.read("uint:10") - 400
    counter = stream.read("uint:14")
    pressure_at_sea = stream.read("uint:8") + 850

    components = [
        f"humidity={humidity}",
        f"core_temp={temp}",
        f"counter={counter}"
    ]

    # 0 means no data received, 850 means no data measured
    if pressure_at_sea > 850:
        components += [f"pressure_at_sea={pressure_at_sea}"]

    # Offsets are the top of the range for multiple segments of
    # the steeper and steeper line
    # 0; 0 + 4**0 * 64; 0 + 4**0 * 64 + 4**1 * 64;
    # 0 + 4**0 * 64 + 4**1 * 64 + 4**2 * 64; ...
    # The proper third element 320 was too high due rounding and so
    # it was reduced to 318 and affected the next one which was
    # updated from 1344 to 1350 to keep the maximum error at around 2.86%
    co2_offset = [0, 64, 318, 1350]

    # 0xff means no data
    if co2_exp < 3 or co2_value < 63:
        components += [f"co2={250 + 6 * (co2_offset[co2_exp] + co2_value * (4**co2_exp))}"]

    status_line = "alive=1,core_vin={}".format(core_vin)
    return [("sensors", [], ",".join(components)), ("status", [], status_line)]

# GPS node
def decode_109(packet):
    stream = BitStream(packet)
    lat_ns = stream.read("uint:1")
    lat_deg = stream.read("uint:7")
    lat_min = stream.read("uint:6")
    lat_sec = stream.read("uint:6")
    lat_sec_100 = stream.read("uint:7")
    lat_dir = lat_ns * 2 - 1 # a little trick, converts 0/1 to -1/1
    lat = lat_dir * lat_deg + lat_min / 60 + lat_sec / (60*60) + lat_sec_100 / (60*60*100)

    lon_ew = stream.read("uint:1")
    lon_deg = stream.read("uint:8")
    lon_min = stream.read("uint:6")
    lon_sec = stream.read("uint:6")
    lon_sec_100 = stream.read("uint:7")
    lon_dir = lon_ew * 2 - 1 # a little trick, converts 0/1 to -1/1
    lon = lon_dir * lon_deg + lon_min / 60 + lon_sec / (60*60) + lon_sec_100 / (60*60*100)

    t_year = stream.read("uint:7")
    t_month = stream.read("uint:4")
    t_day = stream.read("uint:5")
    t_hour = stream.read("uint:5")
    t_min = stream.read("uint:6")
    t_sec = stream.read("uint:6")

    gps_lock = stream.read("uint:1")
    gps_sats = stream.read("uint:7")
    gps_accuracy = stream.read("uint:8")

    # inject century
    localzone = get_localzone()
    now = datetime.now(tz=localzone)

    t_year += (now.year // 100) * 100

    # check if the year rolled over between collection and processing
    if now.month < t_month:
        t_year -= 1

    # for sanity purposes, make sure the month and days are positive
    t_month = max(t_month, 1)
    t_day = max(t_day, 1)

    # timestamp in ns
    gpstime = datetime(t_year, t_month, t_day,
                       t_hour, t_min, t_sec, tzinfo=pytz.utc)
    gpstimestamp = gpstime.timestamp() * 1e9
    gpsline = " " + str(int(gpstimestamp))

    components = [
        f"lat={lat}",
        f"lon={lon}",
        f"gps_lock={gps_lock}",
        f"gps_sats={gps_sats}",
        f"gps_accuracy={gps_accuracy}"
    ]

    status_line = "alive=1"
    return [("sensors", [f"gps_lock={gps_lock}"], ",".join(components)  + gpsline),
            ("status", [], status_line + gpsline)]

# Water level node
def decode_10a(packet):
    temp, water, air, vin = struct.unpack(">HHHB", packet[:7])
    line = []
    if temp != 0xffff:
        line.append("core_temp={}".format(temp - 1000))
    if water != 0xffff:
        line.append("water_pressure={}".format(water * 10))
    if air != 0xffff:
        line.append("air_pressure={}".format(air * 10))

    status_line = "alive=1,core_vin={}".format(vin + 120)

    lines = []
    if line:
        line = ",".join(line)
        lines.append(("sensors", [], line))
    lines.append(("status", [], status_line))
    return lines

def deescape(data):
    out = bytearray()
    while data:
        c, *data = data
        if c == ord('\\'):
            c, *data = data
            out.append(c ^ 0x20)
        else:
            out.append(c)
    return out


def reconstruct_number(data):
    """Takes LSB first sequence of bytes and converts it to
       number"""
    val = 0
    for b in deescape(data)[::-1]:
        val <<= 8
        val += b
    return val


def hex_decoder(data):
    print(data)
    parts = data.split(b"\x00")
    gw_id = reconstruct_number(parts[0])
    type_id = reconstruct_number(parts[1])
    node_id = reconstruct_number(parts[2])
    packet = deescape(parts[3])

    node_tags_map = {key: value for key, value in load_node_tags(node_id)}
    if "type" in node_tags_map:
        old_type_id = type_id
        type_id = int(node_tags_map["type"])
        print("- overriding type: {} -> {}".format(hex(old_type_id), hex(type_id)))

    if type_id == 0x101:
        records = decode_101(packet)
    elif type_id == 0x102:
        records = decode_102(packet)
    elif type_id == 0x105:
        records = decode_105(packet)
    elif type_id == 0x106:
        records = decode_106(packet)
    elif type_id == 0x108:
        records = decode_105(packet)
    elif type_id == 0x109:
        records = decode_109(packet)
    elif type_id == 0x10a:
        records = decode_10a(packet)
    else:
        records = []

    # RX errors provided
    if len(parts) >= 5:
        errors = reconstruct_number(parts[4])
        records.append(("sensors", [], "golay_errors={}".format(errors)))

    # RSSI provided
    if len(parts) >= 6 and parts[5]:
        rssi = reconstruct_number(parts[5])
        records.append(("sensors", [], f"rssi={-rssi / 2}"))

    node_tags = [f"{key}={value}" for key, value in node_tags_map.items() if key != "type"]

    output = []
    for series, tags, line in records:
        tags = [series, "node={},gw={},type={}".format(node_id, gw_id, hex(type_id))] + tags + node_tags
        output.append(",".join(tags) + " " + line)

    return "\n".join(output)

def decoder(input_line):
    if input_line[0] == ord('{'):
        eop = input_line.find(b"}")
        if eop < 0:
            return
        # HEX encoded packet
        return hex_decoder(input_line[1:eop])
    else:
        # influxdb line packet
        return input_line

async def post(headers, data):
    with requests.post(os.environ["TARGET"],
            headers=headers,
            data=data) as r:
        print("{}: {}".format(r.status_code, r.text))


async def safe_connection_handler(reader, writer):
    try:
        await handle_connection(reader, writer)
        await writer.drain()
    except BaseException as e:
        print(e)
    finally:
        writer.close()
        print('Server connection done')


async def handle_connection(reader, writer):
    # First line specifies the influxdb credentials already encoded using base64
    # Every subsequent line is taken, decoded and sent as POST body to influxdb
    addr = writer.get_extra_info('peername')
    print('Server connection opened by {}'.format(addr))
    auth = await reader.readline()
    headers = {
        "Authorization": "Basic " + auth.decode('ascii').strip(),
        "Content-Type": "application/octet-stream"
    }

    while True:
        l = await reader.readline()
        if len(l) == 0:
            break

        try:
            line = l.strip()
            if not line:
                print('PING from {}'.format(addr))
                continue

            if line.startswith(b"#auth="):
                headers["Authorization"] = "Basic " + line[6:].decode('ascii')
                print('AUTH changed for {}'.format(addr))
                continue

            decoded = decoder(line)
            print(decoded)
            if decoded is None:
                continue
        except Exception as ex:
            print(ex)
            continue

        try:
            await post(headers, decoded)
        except Exception as ex:
            print(ex)


if __name__ == "__main__":
    try:
        sc = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        sc.load_cert_chain('ssl/cert.pem', 'ssl/key.pem')
    except Exception:
        print("!!! SSL not available, use this for debugging only !!!")
        sc = None

    port = int(os.environ["PORT"])
    loop = asyncio.get_event_loop()
    coro = asyncio.start_server(safe_connection_handler, '', port, ssl=sc, loop=loop)
    server = loop.run_until_complete(coro)

    print('Serving on {}'.format(server.sockets[0].getsockname()))
    loop.run_forever()

# vim: sw=4 sts=4 et

