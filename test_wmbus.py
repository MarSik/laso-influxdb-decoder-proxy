# vim: sts=4 sw=4 et

import os
import unittest

import laso_influxdb_decoder_proxy as laso

class TestVMbus(unittest.TestCase):
    def setUp(self):
        self.tz = os.environ.get('TZ', '')
        os.environ['TZ'] = 'Europe/Prague'

    def tearDown(self):
        os.environ['TZ'] = self.tz

    def test_simple_water(self):
        data = (b"{\x06\x00\x05\x01\x00\x01\x00"
                b"\x62\x55\x55\\\x20\\\x20\\\x20\x10}")
        line = """status,node=1,gw=6,type=0x105,medium=hot-water alive=1
sensors,node=1,gw=6,type=0x105,medium=hot-water value=16 2297458800000000000"""

        self.assertEqual(line, laso.decoder(data))

    def test_simple_water_wrong_type_override(self):
        data = (b"{\x06\x00\x01\x01\x00\x01\x00"
                b"\x62\x55\x55\\\x20\\\x20\\\x20\x10}")
        line = """status,node=1,gw=6,type=0x105,medium=hot-water alive=1
sensors,node=1,gw=6,type=0x105,medium=hot-water value=16 2297458800000000000"""

        self.assertEqual(line, laso.decoder(data))

    def test_simple_heat(self):
        data = (b"{\x06\x00\x05\x01\x00\x01\x00"
                b"\x80\x55\x55\x80\xfa\x81\x4a\\\x20\\\x20\\\x20\x10}")
        line = """status,node=1,gw=6,type=0x105,medium=heat alive=1
sensors,node=1,gw=6,type=0x105,medium=heat t1=250,t2=330
sensors,node=1,gw=6,type=0x105,medium=heat value=16 2297458800000000000"""
        self.assertEqual(line, laso.decoder(data))

    def test_simple_heat2(self):
        data = (b"{\x00\x05\x01\x00\x90\xd3\x0e\x05\x00\x80&H\x81\x13\x81\x13\\ \\ \x0b@}")
        line = """status,node=84857744,gw=0,type=0x105,medium=heat alive=1
sensors,node=84857744,gw=0,type=0x105,medium=heat t1=275,t2=275
sensors,node=84857744,gw=0,type=0x105,medium=heat value=2880 1549580400000000000"""
        self.assertEqual(line, laso.decoder(data))

    def test_wmbus_gsm_gw(self):
        data = (b"{\x06\x00\x08\x01\x00\xb1\xd3\x0e\x05\x00\x80'\x91\x80\xc9\x80\xc5\\ \\ \\-U\x00\x00}")
        line= """status,node=84857777,gw=6,type=0x108,medium=heat alive=1
sensors,node=84857777,gw=6,type=0x108,medium=heat t1=201,t2=197
sensors,node=84857777,gw=6,type=0x108,medium=heat value=3413 1576537200000000000
sensors,node=84857777,gw=6,type=0x108 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

