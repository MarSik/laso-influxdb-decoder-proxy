# vim: sts=4 sw=4 et

import laso_influxdb_decoder_proxy as laso
import unittest

class TestLasoReports(unittest.TestCase):
    def test_no_golay(self):
        data = b'{\x06\x00\x01\x01\x00\x03\x00\x80\xe6\x89\\ \\ \\*\x08\\ }'
        line = """sensors,node=3,gw=6,type=0x101 core_vin=257,core_temp=230,counter=2568
status,node=3,gw=6,type=0x101 alive=1,core_vin=257"""
        self.assertEqual(line, laso.decoder(data))

    def test_simple_node_report(self):
        data = b'{\x06\x00\x01\x01\x00\x06\x00\x81\\ \xcf\\ \\ \x0cK\x00\x00}'
        line = """sensors,node=6,gw=6,type=0x101 core_vin=327,core_temp=256,counter=3147
status,node=6,gw=6,type=0x101 alive=1,core_vin=327
sensors,node=6,gw=6,type=0x101 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_simple_node_report_rssi(self):
        data = b'{\x06\x00\x01\x01\x00\x06\x00\x81\\ \xcf\\ \\ \x0cK\x00\x00\x41}'
        line = """sensors,node=6,gw=6,type=0x101 core_vin=327,core_temp=256,counter=3147
status,node=6,gw=6,type=0x101 alive=1,core_vin=327
sensors,node=6,gw=6,type=0x101 golay_errors=0
sensors,node=6,gw=6,type=0x101 rssi=-32.5"""
        self.assertEqual(line, laso.decoder(data))

    def test_simple_gw_report(self):
        data = b'{\x06\x00\x02\x01\x00\x06\x00\x18\x03\xa0\x00}'
        line = """sensors,node=6,gw=6,type=0x102 rssi=24,tcp_counter=928
status,node=6,gw=6,type=0x102 alive=1,rssi=24,tcp_counter=928
sensors,node=6,gw=6,type=0x102 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_extended_gw_report(self):
        data = b'{2\x00\x02\x01\x002\x00\\ \\ \\ \\ \\ \\ \\ \\ \\ cL\x82\xb5\x00\x00}'
        line = """sensors,node=50,gw=50,type=0x102 rssi=0,tcp_counter=0
status,node=50,gw=50,type=0x102 alive=1,rssi=0,tcp_counter=0,tcp_failed=0,country=0,network=0,area=25420,cell=33461
sensors,node=50,gw=50,type=0x102 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_environ_node_report(self):
        self.maxDiff = None
        data = b'{\x14\x00\x06\x01\x00)\x00p\x92\xff\x97\xc0\\;\\ \\ \x00\x00}'
        line = """sensors,node=41,gw=20,type=0x106 humidity=560,core_temp=207,counter=27
status,node=41,gw=20,type=0x106 alive=1,core_vin=266
sensors,node=41,gw=20,type=0x106 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_environ_node_report_with_pressure(self):
        self.maxDiff = None
        data = b'{\x14\x00\x06\x01\x00)\x00p\x92\xff\x97\xc0\\;\x80\x80\x00\x00}'
        line = """sensors,node=41,gw=20,type=0x106 humidity=560,core_temp=207,counter=27,pressure_at_sea=978
status,node=41,gw=20,type=0x106 alive=1,core_vin=266
sensors,node=41,gw=20,type=0x106 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_environ_node_report_with_extra_labels(self):
        data = b'{\x14\x00\x06\x01\x00\x17\x00p\x92\xff\x97\xc0\\;\\ \\ \x00\x00}'
        line = """sensors,node=23,gw=20,type=0x106,room=doma humidity=560,core_temp=207,counter=27
status,node=23,gw=20,type=0x106,room=doma alive=1,core_vin=266
sensors,node=23,gw=20,type=0x106,room=doma golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_environ_node_report_with_co2(self):
        self.maxDiff = None
        data = b'{\x14\x00\x06\x01\x00+\x00W\x88#\x95\xc0\x0b\\ \\ \x00\x00}'
        line = """sensors,node=43,gw=20,type=0x106 humidity=435,core_temp=199,counter=11,co2=460
status,node=43,gw=20,type=0x106 alive=1,core_vin=256
sensors,node=43,gw=20,type=0x106 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_gps_node_report(self):
        self.maxDiff = None
        data = b'{\x14\x00\x09\x01\x00\x01\x02\x00\xb1\x2d\xd3\x71\x09\x3a\x94\x5c\x20\x01\x09\xc1\x85\x01\x00\x00}'
        line = """sensors,node=513,gw=20,type=0x109,gps_lock=1 lat=49.19146388888889,lon=16.61631666666667,gps_lock=1,gps_sats=5,gps_accuracy=1 946744741000000000
status,node=513,gw=20,type=0x109 alive=1 946744741000000000
sensors,node=513,gw=20,type=0x109 golay_errors=0"""
        self.assertEqual(line, laso.decoder(data))

    def test_water_level_report(self):
        self.maxDiff = None
        data = b'{2\x00\\*\x01\x00\x00\x04]$\xce$\xd6\xc7\\ \x00\x000}'
        line = """sensors,node=0,gw=50,type=0x10a core_temp=117,water_pressure=94220,air_pressure=94300
status,node=0,gw=50,type=0x10a alive=1,core_vin=319
sensors,node=0,gw=50,type=0x10a golay_errors=0
sensors,node=0,gw=50,type=0x10a rssi=-24.0"""
        self.assertEqual(line, laso.decoder(data))

    def test_water_level_report_noair(self):
        self.maxDiff = None
        data = b'{2\x00\\*\x01\x00\x00\x04]$\xce\xff\xff\xc7\\ \x00\x000}'
        line = """sensors,node=0,gw=50,type=0x10a core_temp=117,water_pressure=94220
status,node=0,gw=50,type=0x10a alive=1,core_vin=319
sensors,node=0,gw=50,type=0x10a golay_errors=0
sensors,node=0,gw=50,type=0x10a rssi=-24.0"""
        self.assertEqual(line, laso.decoder(data))

    def test_water_level_report_nowater(self):
        self.maxDiff = None
        data = b'{2\x00\\*\x01\x00\x00\x04]\xff\xff$\xd6\xc7\\ \x00\x000}'
        line = """sensors,node=0,gw=50,type=0x10a core_temp=117,air_pressure=94300
status,node=0,gw=50,type=0x10a alive=1,core_vin=319
sensors,node=0,gw=50,type=0x10a golay_errors=0
sensors,node=0,gw=50,type=0x10a rssi=-24.0"""
        self.assertEqual(line, laso.decoder(data))

    def test_water_level_report_notemp(self):
        self.maxDiff = None
        data = b'{2\x00\\*\x01\x00\x00\xff\xff$\xce$\xd6\xc7\\ \x00\x000}'
        line = """sensors,node=0,gw=50,type=0x10a water_pressure=94220,air_pressure=94300
status,node=0,gw=50,type=0x10a alive=1,core_vin=319
sensors,node=0,gw=50,type=0x10a golay_errors=0
sensors,node=0,gw=50,type=0x10a rssi=-24.0"""
        self.assertEqual(line, laso.decoder(data))

    def test_water_level_report_nodata(self):
        self.maxDiff = None
        data = b'{2\x00\\*\x01\x00\x00\xff\xff\xff\xff\xff\xff\xc7\\ \x00\x000}'
        line = """status,node=0,gw=50,type=0x10a alive=1,core_vin=319
sensors,node=0,gw=50,type=0x10a golay_errors=0
sensors,node=0,gw=50,type=0x10a rssi=-24.0"""
        self.assertEqual(line, laso.decoder(data))

