FROM python:3.9-alpine3.16

ENV PORT 5443
EXPOSE ${PORT}/tcp

ENV TARGET http://localhost/write?db=iot

RUN mkdir -p /home/ssl /etc/laso
ENV CFGROOT /etc/laso

ADD laso_influxdb_decoder_proxy.py /home/
ADD laso_influxdb_decoder_proxy /home/
ADD Pipfile /home/

ADD test_*.py /home/
ADD test_cfg /home/test_cfg
ADD pytest.ini /home/

RUN apk add --no-cache tzdata

RUN pip install pipenv && cd /home && pipenv install -d && pipenv run pytest && pipenv uninstall --all-dev && rm -rf test_* pytest.ini

WORKDIR /home
ENTRYPOINT pipenv run python ./laso_influxdb_decoder_proxy.py

